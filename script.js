var Counter = 0;

function myCounter() {
 Counter++;   
 document.getElementById("counterDisplay").innerText=Counter;
console.log("vous avez cliqué!" , Counter, " fois");
}

document.getElementById("clickCounter").addEventListener("click", myCounter)

function changeFontSize() {
    var newSize = document.getElementById('textSize').value + "px";
    var paragraphs = document.getElementsByTagName('p');

    for(var i=0; i< paragraphs.length; i++){
        paragraphs[i].style.fontSize = newSize;
    }
}

document.getElementById("textSize").addEventListener('imput' , changeFontSize)